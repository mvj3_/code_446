def resize_and_crop(image, w, h)
      w_original =image[:width]
      h_original =image[:height]
 
      if (w_original*h != h_original * w)
        if w_original*h >= h_original * w
          # long width
          h_original_new = h_original
          w_original_new = h_original_new * (w.to_f / h)
        elsif w_original*h <= h_original * w
          # long height
          w_original_new = w_original
          h_original_new = w_original_new * (h.to_f / w)
        end
      else
         # good proportions
         h_original_new = h_original
         w_original_new = w_original
 
      end
 
 
      # v1. remove extra space
=begin
      if w_original_new != w_original
        remove = ((w_original - w_original_new)/2).round
        image.shave("#{remove}x0")
      end
      if h_original_new != h_original
        remove = ((h_original - h_original_new)/2).round
        image.shave("0x#{remove}")
      end
=end
 
      # v2. or use crop instead of shave
# read about params for crop here: http://www.imagemagick.org/www/command-line-processing.html#geometry
      if w_original_new != w_original || h_original_new != h_original
        x = ((w_original - w_original_new)/2).round
        y = ((h_original - h_original_new)/2).round
        image.crop ("#{w_original_new}x#{h_original_new}+#{x}+#{y}")
      end
 
      #
      image.resize("#{w}x#{h}")
      return image
    end
 
 
# Usage:
 
filename = 'your_filename.jpg'
 
image = MiniMagick::Image.open(filename)
image = Myimages.resize_and_crop(image, 150, 120)
image.write  "output.jpg"